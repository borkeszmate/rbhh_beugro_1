import { createConnection, Connection } from 'mysql2/promise';
import { Product, ConnectionData, PCB } from './types/types';

export class Database {

	connectionData: ConnectionData = {
		host: process.env.DB_HOST,
		user: process.env.DB_USERNAME,
		password: process.env.DB_PASSWORD,
		database: process.env.DB_NAME
	}

	async getProducts(): Promise<Product[]> {
		const connection: Connection = await createConnection(this.connectionData);
		const[rows] = await connection.execute('SELECT * FROM `products`');
		connection.end();

		return JSON.parse(JSON.stringify(rows));
	}

	async saveProduction(values: PCB[]) {
		const queryValues = values.map((item: PCB) => {
			return [
				null,
				item.pcb_id,
				item.quantity,
				item.startDate,
				item.endDate
			]
		});

		const connection: Connection = await createConnection(this.connectionData);
		const query = "INSERT INTO `Production` (id, pcb_id, quantity, startDate, endDate) VALUES ?";
		const[res, err] = await connection.query(query, [queryValues]);
		connection.end();
	}
}

