import * as fs from 'fs';
import * as path from 'path';
import * as os from 'os';

import { PCB } from './types/types';


export class FileReader {

	filePath: string = '';

	constructor() {
		this.filePath = path.resolve(__dirname, '../output/puffer.txt')
	}

	getPuffer() {
		const raw = this.read();
		const parsed = this.parsePuffer(raw);

		return parsed;
	}

	read(): string {
		return fs.readFileSync(this.filePath, 'utf8');
	}

	parsePuffer(text: string): PCB[] {
		const parsed: PCB[] = [];
		const chunks: string[] = text.split(os.EOL).filter((item: string) => item);

		for (const chunk of chunks) {
			const pcb: PCB = {
				pcb_id: 0,
				quantity: 0,
				startDate: '',
				endDate: '',
			};

			const chuckItems = chunk.split(' | ');

			pcb.pcb_id = parseInt(chuckItems[0]);
			pcb.quantity = parseInt(chuckItems[1]);
			pcb.startDate = chuckItems[2];
			pcb.endDate = chuckItems[3];

			parsed.push(pcb);
		}

		return parsed;
	}
}
