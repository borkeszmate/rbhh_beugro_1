import * as fs from 'fs';
import * as path from 'path';
import * as os from 'os';

import { PCB } from './types/types';


export class FileWriter {

	filePath: string = '';

	constructor() {
		this.filePath = path.resolve(__dirname, '../output/puffer.txt')
	}

	write(products: PCB[]) {
		this.clearPuffer();
		const text = this.makeTextPayload(products);

		fs.writeFileSync(this.filePath, text);
	}

	clearPuffer() {
		if (!this.checkFileExistsSync(this.filePath)) {
			fs.openSync(path.resolve(__dirname, '../output/puffer.txt'), 'w');
		} else {
			fs.writeFileSync(this.filePath, '');
		}
	}

	checkFileExistsSync(filepath: string) {
		let flag = true;

		try {
			fs.accessSync(filepath, fs.constants.F_OK);
		} catch (e) {
			flag = false;
		}

		return flag;
	}

	makeTextPayload(products: PCB[]): string {
		let text = '';

		for (const item of products) {
			text += item.pcb_id + ' | ' + item.quantity + ' | ' + item.startDate + ' | ' + item.endDate + os.EOL ;
		}

		return text;
	}
}
