import { PCB } from './types/types';

export class Filter {

	run(arr: PCB[]) {
		let filtered: PCB[] = this.deleteOne(arr, 3);
		filtered = filtered.filter((item: PCB) => item.pcb_id !== 4);

		return filtered;
	}

	deleteOne(arr: any, index: number) {
		arr.splice(index, 1);

		return arr;
	}




}
