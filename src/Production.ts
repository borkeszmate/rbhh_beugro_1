import { Product, PCB } from './types/types';
import { sub, isAfter, isEqual, format } from 'date-fns';
import { generateRandomBetween, removeLetters } from './helpers';

export class Production {

	run(machines: Product[]): PCB[] {
		const produced = [];
		for (const machine of machines) {
			const startDate = this.getStartDate();
			const endDate = this.getEndDate(startDate);

			const item: PCB = {
				pcb_id: parseInt(removeLetters(machine.pcb)),
				quantity: generateRandomBetween(),
				startDate: format(startDate, 'yyyy-LL-dd HH:mm'),
				endDate: format(endDate, 'yyyy-LL-dd HH:mm'),
			};


			produced.push(item);
		}

		return produced;
	}

	getStartDate(): Date {
		return sub(new Date(), {
			minutes: generateRandomBetween(10, 20),
		});
	}

	getEndDate(startDate: Date): Date {
		const endDate = sub(new Date(), {
			minutes: generateRandomBetween(1, 15),
		});

		if (isAfter(startDate, endDate) || isEqual(startDate, endDate)) {
			return this.getEndDate(startDate);
		}

		return endDate;
	}
}
