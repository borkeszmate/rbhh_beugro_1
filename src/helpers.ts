export const generateRandomBetween = (min=1, max = 100) => {
	return Math.floor(Math.random() * (max - min + 1) + min)
}

export const removeLetters = (string: string) => {
	const numbers = string.replace(/[a-zA-Z]/gm, '');

	return numbers;
}
