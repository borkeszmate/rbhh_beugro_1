import './config';

import { Product, PCB } from './types/types';
import { Database } from './Database';
import { Selection } from './Selection';
import { Production } from './Production';
import { FileWriter } from './FileWriter';
import { FileReader } from './FileReader';
import { Filter } from './Filter';

const database = new Database();
const selection = new Selection();
const production = new Production();
const fileWriter = new FileWriter();
const fileReader = new FileReader();
const filter = new Filter();

async function main () {
	console.log('Starting production');

	const products: Product[] = await database.getProducts();
	const selectedSample: Product[] = selection.selectRandom(products, 10);
	const producedItems: PCB[] = production.run(selectedSample);
	fileWriter.write(producedItems);
	const parsed: PCB[] = fileReader.getPuffer();
	const filtered: PCB[] = filter.run(parsed);
	database.saveProduction(filtered);

	console.log('Production ended');
}

main();

