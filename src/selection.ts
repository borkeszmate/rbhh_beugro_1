export class Selection {
	selectRandom(population: any[], pc: number) {
		const result = new Array(pc);
		let len = population.length;
		const taken = new Array(len);
		if (pc > len)
			throw new RangeError("selectRandom: more elements taken than available");
		while (pc--) {
			var x = Math.floor(Math.random() * len);
			result[pc] = population[x in taken ? taken[x] : x];
			taken[x] = --len in taken ? taken[len] : len;
		}
		return result;
	}
}
