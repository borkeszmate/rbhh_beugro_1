export interface Product {
	id: number;
	pcb: string;
}

export interface PCB {
	pcb_id: number;
	quantity: number;
	startDate: string;
	endDate: string;
}

export interface ConnectionData {
	host: string;
	user: string;
	password: string;
	database: string;
}
